import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID
from client.ihm.common.component import Component
from pygame_gui.elements.ui_window import UIWindow
from config import config


class WaitingGamePopup(Component):
    def __init__(self, pygame_manager: pygame_gui.UIManager) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager

        # Monitor Size

        window_width = config.get("monitor")["width"]
        window_height = config.get("monitor")["height"]

        self.set_width(500)
        self.set_height(200)

        self.set_pos_x((window_width * 0.25 + 50))
        self.set_pos_y((window_height * 0.25 + 75))

        self.is_visible = False
        self.title = "En attente d'un adversaire"

    def render(self) -> None:

        self.gui_element: UIWindow = UIWindow(
            pygame.Rect(
                (self.get_pos_x(), self.get_pos_y()),
                (self.get_width(), self.get_height()),
            ),
            manager=self.manager,
            window_display_title=self.title,
        )

        self.label = pygame_gui.elements.UITextBox(
            html_text="Partie XX 15 Tours",
            relative_rect=pygame.Rect((self.get_width() // 2 - 100, 5), (200, 20)),
            manager=self.manager,
            container=self.gui_element,
            wrap_to_height=True,
            object_id=ObjectID(class_id="@game_info"),
        )

        self.button = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect((125, 50), (225, 75)),
            text="Abandonner",
            container=self.gui_element,
            manager=self.manager,
            object_id=ObjectID(class_id="@left_button"),
        )

        self.set_visible(False)

    def set_visible(self, visibility: bool):

        if visibility == True:
            self.gui_element.show()
            self.is_visible = True

        else:
            self.gui_element.hide()
            self.is_visible = False

    def get_is_visible(self) -> bool:
        return self.is_visible
