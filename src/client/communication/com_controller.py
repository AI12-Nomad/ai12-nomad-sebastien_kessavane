from uuid import UUID
from typing import Optional
from client import (
    I_IHMMainCallsComm,
    I_IHMGameCallsComm,
    I_CommCallsIHMGame,
    I_CommCallsData,
    I_CommCallsIHMMain,
    Message,
    IO,
    Move,
    Player,
    LocalGame,
    PublicGame,
    Profile,
    create_client,
)


class ComControllerClient:
    """
    ComController class

    """

    def __init__(self):
        self.__impl_interface_for_ihm_main = IHMGameCallsComm_Impl(self)
        self.__impl_interface_for_ihm_game = IHMMainCallsComm_Impl(self)
        self.__impl_comm_calls_ihm_game = None
        self.__impl_comm_calls_ihm_main = None
        self.__impl_comm_calls_data = None
        self.__callback_map = None
        self.io = None

    def get_interface_for_ihm_main(self) -> I_IHMMainCallsComm:
        return self.__impl_interface_for_ihm_main

    def get_interface_for_ihm_game(self) -> I_IHMGameCallsComm:
        return self.__impl_interface_for_ihm_game

    def set_interface_from_ihm_game(
        self, comm_calls_ihm_game: I_CommCallsIHMGame
    ) -> None:
        self.__impl_comm_calls_ihm_game = comm_calls_ihm_game

    def set_interface_from_data(self, comm_calls_data: I_CommCallsData) -> None:
        self.__impl_comm_calls_data = comm_calls_data

    def set_interface_from_ihm_main(
        self, comm_calls_ihm_main: I_CommCallsIHMMain
    ) -> None:
        self.__impl_comm_calls_ihm_main = comm_calls_ihm_main

    def set_callback_map(self) -> None:
        "a faire quand on aura la connectio faite"
        pass

    def set_connexion(self, connexion: IO) -> None:
        self.io = connexion


class IHMGameCallsComm_Impl(I_IHMGameCallsComm):
    def __init__(self, controller) -> None:
        super().__init__()
        self.comm_controller = controller

    def send_message(self, message: Message) -> None:
        pass

    def place_tile(self, move: Move) -> None:
        pass

    def quit_spactator_interface(self, user_id: UUID, game_id: UUID) -> None:
        pass


class IHMMainCallsComm_Impl(I_IHMMainCallsComm):
    def __init__(self, controller) -> None:
        super().__init__()
        self.comm_controller = controller

    def remove_game(self, game_id: UUID) -> None:
        pass

    def get_profile_info(self, user_id: UUID) -> None:
        pass

    def create_game(self, local_game: LocalGame) -> None:
        pass

    def update_profile(self, profil: Profile) -> None:
        pass

    def spectate_game(self, user_id: UUID, game_id: UUID) -> None:
        pass

    def join_game(self, user_id: UUID, game_id: UUID) -> None:
        pass

    async def connect_to_server(self, ip: str, port: int) -> None:
        pass

    def disconnect_server(self) -> None:
        pass
