from asyncio.events import AbstractServer
from typing import Dict, Optional, Any
from server import (
    I_CommServerCallsData,
    I_IHMServerCallsComm,
    create_server,
    IO,
    LocalGame,
    Profile,
)


class ComController:
    """
    ComController class

    """

    def __init__(self) -> None:
        self.__interface_ihm_server = IHMServerCallsComm_Impl(self)
        self.__impl_interface_for_ihm_server: IHMServerCallsComm_Impl = (
            IHMServerCallsComm_Impl(self)
        )
        self.__impl_comm_calls_data: Optional[I_CommServerCallsData] = None
        self.__server: Optional[AbstractServer] = None

    def get_interface_for_ihm_server(self) -> I_IHMServerCallsComm:
        return self.__impl_interface_for_ihm_server

    def set_interface_from_data(self, comm_calls_data: Optional[I_CommServerCallsData]):
        self.__impl_comm_calls_data = comm_calls_data

    def get_impl_comm_calls_data(self) -> I_CommServerCallsData:
        assert self.__impl_comm_calls_data is not None
        return self.__impl_comm_calls_data

    def get_server(self) -> AbstractServer:
        assert self.__server is not None
        return self.__server

    async def init_server(self, port: int, ip: str = "localhost") -> None:
        self.__server = await create_server(port, ip)
        await self.get_server().serve_forever()


class IHMServerCallsComm_Impl(I_IHMServerCallsComm):
    def __init__(self, controller: ComController) -> None:
        super().__init__()
        self.comm_controller = controller

    def quit_server(self) -> None:
        pass

    async def start_server(self, port: int) -> None:
        self.comm_controller.init_server(port)
